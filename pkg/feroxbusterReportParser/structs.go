package feroxbusterReportParser

type FeroxbusterReport struct {
	Configuration *Configuration
	Statistics    *Statistics
	LogMessages   *[]LogMessage
	HttpResponses *[]HttpResponse
}

type Configuration struct {
	Type              string            `json:"type"`
	Wordlist          string            `json:"wordlist"`
	Config            string            `json:"config"`
	Proxy             string            `json:"proxy"`
	ReplayProxy       string            `json:"replay_proxy"`
	ServerCerts       []interface{}     `json:"server_certs"`
	ClientCert        string            `json:"client_cert"`
	ClientKey         string            `json:"client_key"`
	TargetURL         string            `json:"target_url"`
	StatusCodes       []int             `json:"status_codes"`
	ReplayCodes       []int             `json:"replay_codes"`
	FilterStatus      []interface{}     `json:"filter_status"`
	Threads           int               `json:"threads"`
	Timeout           int               `json:"timeout"`
	Verbosity         int               `json:"verbosity"`
	Silent            bool              `json:"silent"`
	Quiet             bool              `json:"quiet"`
	AutoBail          bool              `json:"auto_bail"`
	AutoTune          bool              `json:"auto_tune"`
	JSON              bool              `json:"json"`
	Output            string            `json:"output"`
	DebugLog          string            `json:"debug_log"`
	UserAgent         string            `json:"user_agent"`
	RandomAgent       bool              `json:"random_agent"`
	Redirects         bool              `json:"redirects"`
	Insecure          bool              `json:"insecure"`
	Extensions        []interface{}     `json:"extensions"`
	Methods           []string          `json:"methods"`
	Data              []interface{}     `json:"data"`
	Headers           map[string]string `json:"headers"`
	Queries           []interface{}     `json:"queries"`
	NoRecursion       bool              `json:"no_recursion"`
	ExtractLinks      bool              `json:"extract_links"`
	AddSlash          bool              `json:"add_slash"`
	Stdin             bool              `json:"stdin"`
	Depth             int               `json:"depth"`
	ScanLimit         int               `json:"scan_limit"`
	Parallel          int               `json:"parallel"`
	RateLimit         int               `json:"rate_limit"`
	FilterSize        []interface{}     `json:"filter_size"`
	FilterLineCount   []interface{}     `json:"filter_line_count"`
	FilterWordCount   []interface{}     `json:"filter_word_count"`
	FilterRegex       []interface{}     `json:"filter_regex"`
	DontFilter        bool              `json:"dont_filter"`
	Resumed           bool              `json:"resumed"`
	ResumeFrom        string            `json:"resume_from"`
	SaveState         bool              `json:"save_state"`
	TimeLimit         string            `json:"time_limit"`
	FilterSimilar     []interface{}     `json:"filter_similar"`
	URLDenylist       []string          `json:"url_denylist"`
	RegexDenylist     []interface{}     `json:"regex_denylist"`
	CollectExtensions bool              `json:"collect_extensions"`
	DontCollect       []string          `json:"dont_collect"`
	CollectBackups    bool              `json:"collect_backups"`
	BackupExtensions  []string          `json:"backup_extensions"`
	CollectWords      bool              `json:"collect_words"`
	ForceRecursion    bool              `json:"force_recursion"`
	ScannerType       string            `json:"scanner_type"`
}

type Statistics struct {
	Type                string    `json:"type"`
	Timeouts            int       `json:"timeouts"`
	Requests            int       `json:"requests"`
	ExpectedPerScan     int       `json:"expected_per_scan"`
	TotalExpected       int       `json:"total_expected"`
	Errors              int       `json:"errors"`
	Successes           int       `json:"successes"`
	Redirects           int       `json:"redirects"`
	ClientErrors        int       `json:"client_errors"`
	ServerErrors        int       `json:"server_errors"`
	TotalScans          int       `json:"total_scans"`
	InitialTargets      int       `json:"initial_targets"`
	LinksExtracted      int       `json:"links_extracted"`
	ExtensionsCollected int       `json:"extensions_collected"`
	Status200s          int       `json:"status_200s"`
	Status301s          int       `json:"status_301s"`
	Status302s          int       `json:"status_302s"`
	Status401s          int       `json:"status_401s"`
	Status403s          int       `json:"status_403s"`
	Status429s          int       `json:"status_429s"`
	Status500s          int       `json:"status_500s"`
	Status503s          int       `json:"status_503s"`
	Status504s          int       `json:"status_504s"`
	Status508s          int       `json:"status_508s"`
	WildcardsFiltered   int       `json:"wildcards_filtered"`
	ResponsesFiltered   int       `json:"responses_filtered"`
	ResourcesDiscovered int       `json:"resources_discovered"`
	URLFormatErrors     int       `json:"url_format_errors"`
	RedirectionErrors   int       `json:"redirection_errors"`
	ConnectionErrors    int       `json:"connection_errors"`
	RequestErrors       int       `json:"request_errors"`
	DirectoryScanTimes  []float64 `json:"directory_scan_times"`
	TotalRuntime        []float64 `json:"total_runtime"`
	Targets             []string  `json:"targets"`
}

type LogMessage struct {
	Type       string  `json:"type"`
	Message    string  `json:"message"`
	Level      string  `json:"level"`
	TimeOffset float64 `json:"time_offset"`
	Module     string  `json:"module"`
}

type HttpResponse struct {
	Type          string      `json:"type"`
	URL           string      `json:"url"`
	OriginalURL   string      `json:"original_url"`
	Path          string      `json:"path"`
	Wildcard      bool        `json:"wildcard"`
	Status        int         `json:"status"`
	Method        string      `json:"method"`
	ContentLength int         `json:"content_length"`
	LineCount     int         `json:"line_count"`
	WordCount     int         `json:"word_count"`
	Headers       HttpHeaders `json:"headers"`
	Extension     string      `json:"extension"`
}

type HttpHeaders struct {
	AcceptRanges  string `json:"accept-ranges"`
	Date          string `json:"date"`
	ContentLength string `json:"content-length"`
	ContentType   string `json:"content-type"`
	Server        string `json:"server"`
	LastModified  string `json:"last-modified"`
	Etag          string `json:"etag"`
	Vary          string `json:"vary"`
}
