package feroxbusterReportParser

import (
	"reflect"
	"testing"
)

var (
	SampleRequests []HttpResponse = generateSampleRequests()
	includeFilters []HttpResponse = generateIncludeFilters()
	excludeFilters []HttpResponse = generateExcludeFilters()
)

func Test_matchesHttpHeadersFilter(t *testing.T) {
	type args struct {
		headers *HttpHeaders
		filter  *HttpHeaders
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// Test case 1: Match request with header Server 'nginx 1.19'
		{
			name: "Match request with header Server 'nginx 1.19'",
			args: args{
				headers: &SampleRequests[0].Headers,
				filter: &HttpHeaders{
					Server: "nginx 1.19",
				},
			},
			want: true,
		},
		// Test case 2: Do not match request with header Server 'nginx 1.19'
		{
			name: "Do not match request with header Server 'nginx 1.19'",
			args: args{
				headers: &SampleRequests[2].Headers,
				filter: &HttpHeaders{
					Server: "nginx 1.19",
				},
			},
			want: false,
		},
		// Test case 3: Match request with header Server 'nginx 1.19' and header Content-Type 'application/json'
		{
			name: "Match request with header Server 'nginx 1.19' and header Content-Type 'application/json'",
			args: args{
				headers: &SampleRequests[0].Headers,
				filter: &HttpHeaders{
					Server:      "nginx 1.19",
					ContentType: "application/json",
				},
			},
			want: true,
		},
		// Test case 4: Do not match request with header Server 'nginx 1.19' and header Content-Type 'application/json'
		{
			name: "Do not match request with header Server 'nginx 1.19' and header Content-Type 'application/json'",
			args: args{
				headers: &SampleRequests[1].Headers,
				filter: &HttpHeaders{
					Server:      "nginx 1.19",
					ContentType: "application/json",
				},
			},
			want: false,
		}}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := matchesHttpHeadersFilter(tt.args.headers, tt.args.filter); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("matchesHttpHeadersFilter() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_matchesHttpResponseFilter(t *testing.T) {
	type args struct {
		response *HttpResponse
		filter   *HttpResponse
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// Test case 1: Match request with status code '200'
		{
			name: "Match request with status code '200'",
			args: args{
				response: &SampleRequests[0],
				filter: &HttpResponse{
					Status: 200,
				},
			},
			want: true,
		},
		// Test case 2: Do not match request with status code '200'
		{
			name: "Do not match request with status code '200'",
			args: args{
				response: &SampleRequests[2],
				filter: &HttpResponse{
					Status: 200,
				},
			},
			want: false,
		},
		// Test case 3: Match request with status code '200' and path 'index.html'
		{
			name: "Match request with status code '200' and path 'index.html'",
			args: args{
				response: &SampleRequests[0],
				filter: &HttpResponse{
					Status: 200,
					Path:   "/index.html"},
			},
			want: true,
		},
		// Test case 4: Do not match request with status code '200' and path 'index.html'
		{
			name: "Do not match request with status code '200' and path 'index.html'",
			args: args{
				response: &SampleRequests[2],
				filter: &HttpResponse{
					Status: 200,
					Path:   "/index.html",
				},
			},
			want: false,
		}}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := matchesHttpResponseFilter(tt.args.response, tt.args.filter); got != tt.want {
				t.Errorf("matchesHttpResponseFilter() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHttpResponse_DoesMatch(t *testing.T) {
	type fields struct {
		Type          string
		URL           string
		OriginalURL   string
		Path          string
		Wildcard      bool
		Status        int
		Method        string
		ContentLength int
		LineCount     int
		WordCount     int
		Headers       HttpHeaders
		Extension     string
	}
	type args struct {
		filter *HttpResponse
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		// Test case 1: Match request with status code '200' and path 'index.html' and header Server 'nginx 1.19'
		{
			name:   "Match request with status code '200' and path 'index.html' and header Server 'nginx 1.19'",
			fields: fields(SampleRequests[0]),
			args: args{
				filter: &HttpResponse{
					Status: 200,
					Path:   "/index.html",
					Headers: HttpHeaders{
						Server: "nginx 1.19",
					},
				},
			},
			want: true,
		},
		// Test case 2: Do not match request with status code '200' and path 'index.html' and header Server 'nginx 1.19'
		{
			name: "Do not match request with status code '200' and path 'index.html' and header Server 'nginx 1.19''",
			fields: fields{
				Status: 200,
				Path:   "/404.php",
				Headers: HttpHeaders{
					Server: "nginx 1.19",
				},
			},
			args: args{
				filter: &HttpResponse{
					Status: 200,
					Path:   "/index.html",
					Headers: HttpHeaders{
						Server: "nginx 1.19",
					},
				},
			},
			want: false,
		},
		// Test case 3: Do not match 2 request with status code '200' and path 'index.html' and header Server 'nginx 1.19'
		{
			name:   "Do not match 2 request with status code '200' and path 'index.html' and header Server 'nginx 1.19'",
			fields: fields(SampleRequests[1]),
			args: args{
				filter: &HttpResponse{
					Status: 200,
					Path:   "/index.html",
					Headers: HttpHeaders{
						Server: "nginx 1.19",
					},
				},
			},
			want: false,
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			FeroxbusterRequest := &HttpResponse{
				Type:          tt.fields.Type,
				URL:           tt.fields.URL,
				OriginalURL:   tt.fields.OriginalURL,
				Path:          tt.fields.Path,
				Wildcard:      tt.fields.Wildcard,
				Status:        tt.fields.Status,
				Method:        tt.fields.Method,
				ContentLength: tt.fields.ContentLength,
				LineCount:     tt.fields.LineCount,
				WordCount:     tt.fields.WordCount,
				Headers:       tt.fields.Headers,
				Extension:     tt.fields.Extension,
			}
			if got := FeroxbusterRequest.DoesMatch(tt.args.filter); got != tt.want {
				t.Errorf("HttpResponse.DoesMatch() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestApplyFilters(t *testing.T) {
	type args struct {
		FeroxbusterRequests *[]HttpResponse
		includeFilter       *HttpResponse
		excludeFilter       *HttpResponse
	}
	type test struct {
		name string
		args args
		want *[]HttpResponse
	}

	var tests []test
	// Test case 1: Include status 200 and Exclude path /404.php
	tests = append(tests, test{
		name: "Include status 200 and Exclude path /404.php",
		args: args{
			FeroxbusterRequests: &SampleRequests,
			includeFilter:       &includeFilters[0],
			excludeFilter:       &excludeFilters[0],
		},
		want: &[]HttpResponse{SampleRequests[0], SampleRequests[1]},
	})
	// Test case 2: Include status 200, method GET, CL 814 and Exclude Content Type application/json
	tests = append(tests, test{
		name: "Include status 200, method GET, CL 814 and Exclude Content Type application/json",
		args: args{
			FeroxbusterRequests: &SampleRequests,
			includeFilter:       &includeFilters[1],
			excludeFilter:       &excludeFilters[1],
		},
		want: &[]HttpResponse{SampleRequests[1]},
	})
	// Test case 3: Include status 200, Server 'Apache 2.4.7' and Exclude path 404.php, status 404
	tests = append(tests, test{
		name: "Include status 200, Server 'Apache 2.4.7' and Exclude path 404.php, status 404",
		args: args{
			FeroxbusterRequests: &SampleRequests,
			includeFilter:       &includeFilters[2],
			excludeFilter:       &excludeFilters[2],
		},
		want: &[]HttpResponse{SampleRequests[1]},
	})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ApplyFilters(tt.args.FeroxbusterRequests, tt.args.includeFilter, tt.args.excludeFilter); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ApplyFilters() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Function to generate sample requests
func generateSampleRequests() []HttpResponse {
	// Define sample requests
	request1 := HttpResponse{
		Type:          "Type1",
		URL:           "URL1",
		OriginalURL:   "OriginalURL1",
		Path:          "/index.html",
		Wildcard:      false,
		Status:        200,
		Method:        "GET",
		ContentLength: 100,
		LineCount:     50,
		WordCount:     20,
		Headers: HttpHeaders{
			AcceptRanges:  "AcceptRanges1",
			Date:          "Date1",
			ContentLength: "814",
			ContentType:   "application/json",
			Server:        "nginx 1.19",
			LastModified:  "LastModified1",
			Etag:          "Etag1",
			Vary:          "Vary1",
		},
		Extension: "Extension1",
	}

	// Define sample requests
	request2 := HttpResponse{
		Type:          "Type1",
		URL:           "URL1",
		OriginalURL:   "OriginalURL1",
		Path:          "/index.html",
		Wildcard:      false,
		Status:        200,
		Method:        "GET",
		ContentLength: 100,
		LineCount:     50,
		WordCount:     20,
		Headers: HttpHeaders{
			AcceptRanges:  "AcceptRanges1",
			Date:          "Date1",
			ContentLength: "814",
			ContentType:   "multipart/form-data",
			Server:        "Apache 2.4.7",
			LastModified:  "LastModified1",
			Etag:          "Etag1",
			Vary:          "Vary1",
		},
		Extension: "Extension1",
	}

	request3 := HttpResponse{
		Type:          "Type2",
		URL:           "URL2",
		OriginalURL:   "OriginalURL2",
		Path:          "/404.php",
		Wildcard:      true,
		Status:        404,
		Method:        "POST",
		ContentLength: 150,
		LineCount:     70,
		WordCount:     30,
		Headers: HttpHeaders{
			AcceptRanges:  "AcceptRanges2",
			Date:          "Date2",
			ContentLength: "404",
			ContentType:   "text/html",
			Server:        "Apache 2.4.7",
			LastModified:  "LastModified2",
			Etag:          "Etag2",
			Vary:          "Vary2",
		},
		Extension: "Extension2",
	}

	// Return sample requests
	return []HttpResponse{request1, request2, request3}
}

// Function to generate an inclusion filter
func generateIncludeFilters() []HttpResponse {
	// Define inclusion filter
	f1 := &HttpResponse{
		Status: 200,
	}
	f2 := &HttpResponse{
		Status: 200,
		Method: "GET",
		Headers: HttpHeaders{
			ContentLength: "814",
		},
	}
	f3 := &HttpResponse{
		Status: 200,
		Headers: HttpHeaders{
			Server: "Apache 2.4.7",
		},
	}

	return []HttpResponse{*f1, *f2, *f3}
}

// Function to generate an exclusion filter
func generateExcludeFilters() []HttpResponse {
	// Define exclusion filter
	f1 := &HttpResponse{
		Path: "/404.php",
	}
	f2 := &HttpResponse{
		Headers: HttpHeaders{
			ContentType: "application/json",
		},
	}
	f3 := &HttpResponse{
		Status: 404,
		Path:   "/404.php",
	}

	return []HttpResponse{*f1, *f2, *f3}
}
