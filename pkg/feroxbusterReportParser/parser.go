package feroxbusterReportParser

import (
	"encoding/json"
	"io"
	"log"
	"strings"
)

// feroxbusterReportParser parses Feroxbuster Json outputs into tags structs

// Public Scope

func Parse(jsonInput string) *FeroxbusterReport {
	return &FeroxbusterReport{
		Configuration: ParseConfiguration(jsonInput),
		Statistics:    ParseStatistics(jsonInput),
		LogMessages:   ParseLogMessages(jsonInput),
		HttpResponses: ParseHttpResponses(jsonInput),
	}
}

func ParseConfiguration(jsonInput string) *Configuration {
	configuration := *parse[Configuration](jsonInput)
	// remove bad input from array
	var clearedConfiguration []Configuration
	for i := range configuration {
		if configuration[i].Type == "configuration" {
			clearedConfiguration = append(clearedConfiguration, configuration[i])
		}
	}

	if len(clearedConfiguration) == 0 {
		return &Configuration{}
	}
	return &clearedConfiguration[0]
}

func ParseStatistics(jsonInput string) *Statistics {
	statistics := *parse[Statistics](jsonInput)

	// remove bad input from array
	var clearedStatistics []Statistics
	for i := range statistics {
		if statistics[i].Type == "statistics" {
			clearedStatistics = append(clearedStatistics, statistics[i])
		}
	}

	if len(clearedStatistics) == 0 {
		return &Statistics{}
	}
	return &clearedStatistics[0]
}

func ParseLogMessages(jsonInput string) *[]LogMessage {
	logMessages := *parse[LogMessage](jsonInput)

	// remove bad input from array
	var clearedLogMessages []LogMessage
	for i := range logMessages {
		if logMessages[i].Type == "log" {
			clearedLogMessages = append(clearedLogMessages, logMessages[i])
		}
	}

	return &clearedLogMessages
}

func ParseHttpResponses(jsonInput string) *[]HttpResponse {
	httpResponses := *parse[HttpResponse](jsonInput)

	// remove bad input from array
	var clearedHttpResponses []HttpResponse
	for i := range httpResponses {
		if httpResponses[i].Type == "response" {
			clearedHttpResponses = append(clearedHttpResponses, httpResponses[i])
		}
	}

	return &clearedHttpResponses
}

// Private Scope

func parse[T any](jsonData string) *[]T {
	dec := json.NewDecoder(strings.NewReader(string(jsonData)))
	var data *[]T = &[]T{}
	for {
		var occurence T
		err := dec.Decode(&occurence)
		if err == io.EOF {
			// all done
			break
		}

		if err != nil && (strings.HasSuffix(err.Error(), "cannot unmarshal bool into Go struct field Statistics.redirects of type int") ||
			strings.HasSuffix(err.Error(), "cannot unmarshal number into Go struct field Configuration.redirects of type bool")) {
			continue
		}

		if err != nil {
			log.Fatal(err)
		}

		*data = append(*data, occurence)
	}

	return data
}
