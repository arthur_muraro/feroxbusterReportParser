package feroxbusterReportParser

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func GetJsonInput(config, stats, logs, responses bool) string {
	var report string
	if config {
		report += `{"type":"configuration","wordlist":"/proc/self/fd/11","config":"","proxy":"","replay_proxy":"","server_certs":[],"client_cert":"","client_key":"","target_url":"https://api.example.com/target","status_codes":[100,101,102,200,201,202,203,204,205,206,207,208,226,300,301,302,303,304,305,307,308,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,421,422,423,424,426,428,429,431,451,500,501,502,503,504,505,506,507,508,510,511,103,425],"replay_codes":[100,101,102,200,201,202,203,204,205,206,207,208,226,300,301,302,303,304,305,307,308,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,421,422,423,424,426,428,429,431,451,500,501,502,503,504,505,506,507,508,510,511,103,425],"filter_status":[],"threads":50,"timeout":7,"verbosity":0,"silent":false,"quiet":false,"auto_bail":false,"auto_tune":false,"json":true,"output":"../loots/ferox_methods.json","debug_log":"","user_agent":"feroxbuster/2.10.3","random_agent":false,"redirects":false,"insecure":false,"extensions":[],"methods":["OPTIONS","GET","HEAD","POST","PUT","DELETE","TRACE","CONNECT","PROPFIND","PROPPATCH","MKCOL","COPY","MOVE","LOCK","UNLOCK","VERSION-CONTROL","REPORT","CHECKOUT","CHECKIN","UNCHECKOUT","MKWORKSPACE","UPDATE","LABEL","MERGE","BASELINE-CONTROL","MKACTIVITY","ORDERPATCH","ACL","PATCH","SEARCH","BCOPY","BDELETE","BMOVE","BPROPFIND","BPROPPATCH","NOTIFY","POLL","SUBSCRIBE","UNSUBSCRIBE","X-MS-ENUMATTS","RPC_OUT_DATA","RPC_IN_DATA","DEBUG","THIS_METHOD_SURELY_DOES_NOT_EXISTS"],"data":[],"headers":{"Authorization":"Bearer <REDACTED>","x-api-key":"<REDACTED>"},"queries":[],"no_recursion":false,"extract_links":false,"add_slash":false,"stdin":false,"depth":4,"scan_limit":0,"parallel":0,"rate_limit":0,"filter_size":[],"filter_line_count":[],"filter_word_count":[],"filter_regex":[],"dont_filter":true,"resumed":false,"resume_from":"","save_state":true,"time_limit":"","filter_similar":[],"url_denylist":["https://api.example.com/target"],"regex_denylist":[],"collect_extensions":false,"dont_collect":["tif","tiff","ico","cur","bmp","webp","svg","png","jpg","jpeg","jfif","gif","avif","apng","pjpeg","pjp","mov","wav","mpg","mpeg","mp3","mp4","m4a","m4p","m4v","ogg","webm","ogv","oga","flac","aac","3gp","css","zip","xls","xml","gz","tgz"],"collect_backups":false,"backup_extensions":["~",".bak",".bak2",".old",".1"],"collect_words":false,"force_recursion":false}`
	}
	if stats {
		report += `{"type":"statistics","timeouts":1,"requests":50,"expected_per_scan":90,"total_expected":90,"errors":2,"successes":5,"redirects":1,"client_errors":48,"server_errors":1,"total_scans":2,"initial_targets":1,"links_extracted":2,"extensions_collected":1,"status_200s":5,"status_301s":1,"status_302s":1,"status_401s":1,"status_403s":1,"status_429s":5,"status_500s":1,"status_503s":1,"status_504s":1,"status_508s":1,"wildcards_filtered":1,"responses_filtered":1,"resources_discovered":50,"url_format_errors":1,"redirection_errors":1,"connection_errors":1,"request_errors":2,"directory_scan_times":[6.5],"total_runtime":[7.5],"targets":["https://api.anonymized.example.com/"]}`
		report += `{"type":"statistics","timeouts":0,"requests":55,"expected_per_scan":95,"total_expected":95,"errors":1,"successes":4,"redirects":0,"client_errors":50,"server_errors":0,"total_scans":3,"initial_targets":0,"links_extracted":1,"extensions_collected":0,"status_200s":4,"status_301s":0,"status_302s":0,"status_401s":0,"status_403s":0,"status_429s":4,"status_500s":0,"status_503s":0,"status_504s":0,"status_508s":0,"wildcards_filtered":0,"responses_filtered":0,"resources_discovered":55,"url_format_errors":0,"redirection_errors":0,"connection_errors":0,"request_errors":1,"directory_scan_times":[6.0],"total_runtime":[7.0],"targets":["https://api.anonymized.example.net/"]}`
	}
	if logs {
		report += `{"type":"log","message":"a","level":"a","time_offset":0.0,"module":"a"}`
	}
	if responses {
		report += `{"type":"response","url":"a","original_url":"a","path":"a","wildcard":false,"status":200,"method":"GET","content_length":935,"line_count":5,"word_count":13,"headers":{"date":"a","etag":"a","content-type":"a","server":"a","last-modified":"a","accept-ranges":"a","content-length":"529"},"extension":""}`
		report += `{"type":"response","url":"a","original_url":"a","path":"a","wildcard":false,"status":200,"method":"GET","content_length":4796,"line_count":67,"word_count":274,"headers":{"content-type":"a","last-modified":"a","server":"a","content-length":"4796","date":"a","accept-ranges":"a","etag":"a"},"extension":""}`
		report += `{"type":"response","url":"a","original_url":"a","path":"a","wildcard":false,"status":200,"method":"GET","content_length":582,"line_count":37,"word_count":79,"headers":{"vary":"a","content-type":"a","last-modified":"a","etag":"a","content-length":"582","accept-ranges":"a","server":"a","date":"a"},"extension":""}`
		report += `{"type":"response","url":"a","original_url":"a","path":"a","wildcard":false,"status":200,"method":"GET","content_length":2235,"line_count":76,"word_count":201,"headers":{"server":"a","accept-ranges":"a","content-length":"2235","date":"a","etag":"a","last-modified":"a","vary":"a","content-type":"a"},"extension":""}`

	}
	return report
}

func GetSerializedReport(config, stats, logs, responses bool) *FeroxbusterReport {
	report := &FeroxbusterReport{}

	if config {
		report.Configuration = &Configuration{Type: "configuration", Wordlist: "/proc/self/fd/11", Config: "", Proxy: "", ReplayProxy: "", ServerCerts: []interface{}{}, ClientCert: "", ClientKey: "", TargetURL: "https://api.example.com/target", StatusCodes: []int{100, 101, 102, 200, 201, 202, 203, 204, 205, 206, 207, 208, 226, 300, 301, 302, 303, 304, 305, 307, 308, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 421, 422, 423, 424, 426, 428, 429, 431, 451, 500, 501, 502, 503, 504, 505, 506, 507, 508, 510, 511, 103, 425}, ReplayCodes: []int{100, 101, 102, 200, 201, 202, 203, 204, 205, 206, 207, 208, 226, 300, 301, 302, 303, 304, 305, 307, 308, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 421, 422, 423, 424, 426, 428, 429, 431, 451, 500, 501, 502, 503, 504, 505, 506, 507, 508, 510, 511, 103, 425}, FilterStatus: []interface{}{}, Threads: 50, Timeout: 7, Verbosity: 0, Silent: false, Quiet: false, AutoBail: false, AutoTune: false, JSON: true, Output: "../loots/ferox_methods.json", DebugLog: "", UserAgent: "feroxbuster/2.10.3", RandomAgent: false, Redirects: false, Insecure: false, Extensions: []interface{}{}, Methods: []string{"OPTIONS", "GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "CONNECT", "PROPFIND", "PROPPATCH", "MKCOL", "COPY", "MOVE", "LOCK", "UNLOCK", "VERSION-CONTROL", "REPORT", "CHECKOUT", "CHECKIN", "UNCHECKOUT", "MKWORKSPACE", "UPDATE", "LABEL", "MERGE", "BASELINE-CONTROL", "MKACTIVITY", "ORDERPATCH", "ACL", "PATCH", "SEARCH", "BCOPY", "BDELETE", "BMOVE", "BPROPFIND", "BPROPPATCH", "NOTIFY", "POLL", "SUBSCRIBE", "UNSUBSCRIBE", "X-MS-ENUMATTS", "RPC_OUT_DATA", "RPC_IN_DATA", "DEBUG", "THIS_METHOD_SURELY_DOES_NOT_EXISTS"}, Data: []interface{}{}, Headers: map[string]string{"Authorization": "Bearer <REDACTED>", "x-api-key": "<REDACTED>"}, Queries: []interface{}{}, NoRecursion: false, ExtractLinks: false, AddSlash: false, Stdin: false, Depth: 4, ScanLimit: 0, Parallel: 0, RateLimit: 0, FilterSize: []interface{}{}, FilterLineCount: []interface{}{}, FilterWordCount: []interface{}{}, FilterRegex: []interface{}{}, DontFilter: true, Resumed: false, ResumeFrom: "", SaveState: true, TimeLimit: "", FilterSimilar: []interface{}{}, URLDenylist: []string{"https://api.example.com/target"}, RegexDenylist: []interface{}{}, CollectExtensions: false, DontCollect: []string{"tif", "tiff", "ico", "cur", "bmp", "webp", "svg", "png", "jpg", "jpeg", "jfif", "gif", "avif", "apng", "pjpeg", "pjp", "mov", "wav", "mpg", "mpeg", "mp3", "mp4", "m4a", "m4p", "m4v", "ogg", "webm", "ogv", "oga", "flac", "aac", "3gp", "css", "zip", "xls", "xml", "gz", "tgz"}, CollectBackups: false, BackupExtensions: []string{"~", ".bak", ".bak2", ".old", ".1"}, CollectWords: false, ForceRecursion: false, ScannerType: ""}
	}
	if stats {
		report.Statistics = &Statistics{Type: "statistics", Timeouts: 1, Requests: 50, ExpectedPerScan: 90, TotalExpected: 90, Errors: 2, Successes: 5, Redirects: 1, ClientErrors: 48, ServerErrors: 1, TotalScans: 2, InitialTargets: 1, LinksExtracted: 2, ExtensionsCollected: 1, Status200s: 5, Status301s: 1, Status302s: 1, Status401s: 1, Status403s: 1, Status429s: 5, Status500s: 1, Status503s: 1, Status504s: 1, Status508s: 1, WildcardsFiltered: 1, ResponsesFiltered: 1, ResourcesDiscovered: 50, URLFormatErrors: 1, RedirectionErrors: 1, ConnectionErrors: 1, RequestErrors: 2, DirectoryScanTimes: []float64{6.5}, TotalRuntime: []float64{7.5}, Targets: []string{"https://api.anonymized.example.com/"}}
	}
	if logs {
		report.LogMessages = &[]LogMessage{{Type: "log", Message: "a", Level: "a", TimeOffset: 0, Module: "a"}}
	}
	if responses {
		report.HttpResponses = &[]HttpResponse{{Type: "response", URL: "a", OriginalURL: "a", Path: "a", Wildcard: false, Status: 200, Method: "GET", ContentLength: 935, LineCount: 5, WordCount: 13, Headers: HttpHeaders{AcceptRanges: "a", Date: "a", ContentLength: "529", ContentType: "a", Server: "a", LastModified: "a", Etag: "a", Vary: ""}, Extension: ""}, {Type: "response", URL: "a", OriginalURL: "a", Path: "a", Wildcard: false, Status: 200, Method: "GET", ContentLength: 4796, LineCount: 67, WordCount: 274, Headers: HttpHeaders{AcceptRanges: "a", Date: "a", ContentLength: "4796", ContentType: "a", Server: "a", LastModified: "a", Etag: "a", Vary: ""}, Extension: ""}, {Type: "response", URL: "a", OriginalURL: "a", Path: "a", Wildcard: false, Status: 200, Method: "GET", ContentLength: 582, LineCount: 37, WordCount: 79, Headers: HttpHeaders{AcceptRanges: "a", Date: "a", ContentLength: "582", ContentType: "a", Server: "a", LastModified: "a", Etag: "a", Vary: "a"}, Extension: ""}, {Type: "response", URL: "a", OriginalURL: "a", Path: "a", Wildcard: false, Status: 200, Method: "GET", ContentLength: 2235, LineCount: 76, WordCount: 201, Headers: HttpHeaders{AcceptRanges: "a", Date: "a", ContentLength: "2235", ContentType: "a", Server: "a", LastModified: "a", Etag: "a", Vary: "a"}, Extension: ""}}
	}

	if report.Configuration == nil {
		report.Configuration = &Configuration{}
	}
	if report.Statistics == nil {
		report.Statistics = &Statistics{}
	}
	if report.LogMessages == nil {
		report.LogMessages = &[]LogMessage{}

	}
	if report.HttpResponses == nil {
		report.HttpResponses = &[]HttpResponse{}

	}

	return report
}

func TestParseStatistics(t *testing.T) {
	type args struct {
		jsonInput string
	}
	tests := []struct {
		name string
		args args
		want *Statistics
	}{
		{
			name: "Filter by statistics",
			args: args{
				jsonInput: GetJsonInput(true, true, true, true),
			},
			want: GetSerializedReport(false, true, false, false).Statistics,
		},
		{
			name: "Filter by statistics with no statistics in report",
			args: args{
				jsonInput: GetJsonInput(true, false, true, true),
			},
			want: &Statistics{},
		}}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseStatistics(tt.args.jsonInput); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseStatistics() = %#v, want %#v", got, tt.want)
			}
		})
	}
}

func TestParseLogMessages(t *testing.T) {
	type args struct {
		jsonInput string
	}
	tests := []struct {
		name string
		args args
		want *[]LogMessage
	}{
		{
			name: "Filter by log messages",
			args: args{
				jsonInput: GetJsonInput(true, true, true, true),
			},
			want: &[]LogMessage{{Type: "log", Message: "a", Level: "a", TimeOffset: 0, Module: "a"}},
		}}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseLogMessages(tt.args.jsonInput); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseLogMessages() = %#v, want %#v", got, tt.want)
			}
		})
	}

	t.Run("Filter by log messages with no log messages in report", func(t *testing.T) {
		if got := ParseLogMessages(GetJsonInput(true, true, false, true)); !reflect.DeepEqual(len(*got), len([]LogMessage{})) {
			t.Errorf("ParseLogMessages() = %#v, want %#v", got, &[]LogMessage{})
		}
	})
}

func TestParseHttpResponses(t *testing.T) {
	type args struct {
		jsonInput string
	}
	tests := []struct {
		name string
		args args
		want *[]HttpResponse
	}{
		{
			name: "Filter by HTTP responses",
			args: args{
				jsonInput: GetJsonInput(true, true, true, true),
			},
			want: GetSerializedReport(false, false, false, true).HttpResponses,
		}}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseHttpResponses(tt.args.jsonInput); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseHttpResponses() = %#v, want %#v", got, tt.want)
			}
		})
	}

	t.Run("Filter by http responses with no http responses in report", func(t *testing.T) {
		if got := ParseLogMessages(GetJsonInput(true, true, false, true)); !reflect.DeepEqual(len(*got), len([]HttpResponse{})) {
			t.Errorf("ParseHttpResponses() = %#v, want %#v", got, &[]HttpResponse{})
		}
	})
}

func TestParseConfiguration(t *testing.T) {
	type args struct {
		jsonInput string
	}
	tests := []struct {
		name string
		args args
		want *Configuration
	}{
		{
			name: "Filter by configuration",
			args: args{
				jsonInput: GetJsonInput(true, true, true, true),
			},
			want: GetSerializedReport(true, false, false, false).Configuration,
		}}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseConfiguration(tt.args.jsonInput); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseConfiguration() = %#v, want %#v", got, tt.want)
			}
		})
	}

	t.Run("Filter by configuration with no configuration in report", func(t *testing.T) {
		if got := ParseConfiguration(GetJsonInput(false, true, true, true)); !reflect.DeepEqual(*got, Configuration{}) {
			t.Errorf("ParseConfiguration() = %#v, want %#v", got, &Configuration{})
		}
	})
}

func TestParse(t *testing.T) {
	type args struct {
		jsonInput string
	}
	tests := []struct {
		name string
		args args
		want *FeroxbusterReport
	}{
		{
			name: "Parse complete Report",
			args: args{
				jsonInput: GetJsonInput(true, true, true, true),
			},
			want: GetSerializedReport(true, true, true, true),
		},
		{
			name: "Parse Report with no Configuration",
			args: args{
				jsonInput: GetJsonInput(false, true, true, true),
			},
			want: GetSerializedReport(false, true, true, true),
		},
		{
			name: "Parse Report with no Statistics",
			args: args{
				jsonInput: GetJsonInput(true, false, true, true),
			},
			want: GetSerializedReport(true, false, true, true),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Parse(tt.args.jsonInput); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parse() = %#v, want %#v", got, tt.want)
			}
		})
	}

	t.Run("Parse Report with no LogMessages", func(t *testing.T) {
		got := Parse(GetJsonInput(true, true, false, true))
		want := GetSerializedReport(true, true, false, true)
		if !reflect.DeepEqual(got.Configuration, want.Configuration) {
			t.Errorf("Parse() Configuration = %#v, want %#v", got.Configuration, want.Configuration)
		}
		if !reflect.DeepEqual(got.Statistics, want.Statistics) {
			t.Errorf("Parse() Statistics = %#v, want %#v", got.Statistics, want.Statistics)
		}
		if !reflect.DeepEqual(got.HttpResponses, want.HttpResponses) {
			t.Errorf("Parse() HttpResponses = %#v, want %#v", got.HttpResponses, want.HttpResponses)
		}

		if len(*got.LogMessages) != 0 || !assert.NotNil(t, got.LogMessages) {
			t.Errorf("Parse() LogMessages = %#v, want empty slice", got.LogMessages)
		}
	})

	t.Run("Parse Report with no HttpResponses", func(t *testing.T) {
		got := Parse(GetJsonInput(true, true, true, false))
		want := GetSerializedReport(true, true, true, false)
		if !reflect.DeepEqual(got.Configuration, want.Configuration) {
			t.Errorf("Parse() Configuration = %#v, want %#v", got.Configuration, want.Configuration)
		}
		if !reflect.DeepEqual(got.Statistics, want.Statistics) {
			t.Errorf("Parse() Statistics = %#v, want %#v", got.Statistics, want.Statistics)
		}
		if !reflect.DeepEqual(got.LogMessages, want.LogMessages) {
			t.Errorf("Parse() LogMessages = %#v, want %#v", got.LogMessages, want.LogMessages)
		}

		if len(*got.HttpResponses) != 0 || !assert.NotNil(t, got.HttpResponses) {
			t.Errorf("Parse() HttpResponses = %#v, want empty slice", got.HttpResponses)
		}
	})

	t.Run("Parse Report with empty input", func(t *testing.T) {
		got := Parse(GetJsonInput(false, false, false, false))
		want := GetSerializedReport(false, false, false, false)
		if !reflect.DeepEqual(got.Configuration, want.Configuration) {
			t.Errorf("Parse() Configuration = %#v, want %#v", got.Configuration, want.Configuration)
		}
		if !reflect.DeepEqual(got.Statistics, want.Statistics) {
			t.Errorf("Parse() Statistics = %#v, want %#v", got.Statistics, want.Statistics)
		}

		if len(*got.LogMessages) != 0 || !assert.NotNil(t, got.LogMessages) {
			t.Errorf("Parse() LogMessages = %#v, want empty slice", got.LogMessages)
		}

		if len(*got.HttpResponses) != 0 || !assert.NotNil(t, got.HttpResponses) {
			t.Errorf("Parse() HttpResponses = %#v, want empty slice", got.HttpResponses)
		}
	})
}
