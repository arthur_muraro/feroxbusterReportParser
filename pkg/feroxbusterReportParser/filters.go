package feroxbusterReportParser

// public scope

// ApplyFilters filters an array of HttpResponse structs based on inclusion and exclusion filters.
// It iterates over each HttpResponse in the input slice and checks if it matches the inclusion and exclusion filters.
// If a request matches the inclusion filter but does not match the exclusion filter, it is included in the filtered result.
// Parameters:
//   - FeroxbusterRequests: A pointer to a slice of HttpResponse structs to be filtered.
//   - includeFilter: A pointer to an HttpResponse struct representing the inclusion filter.
//   - excludeFilter: A pointer to an HttpResponse struct representing the exclusion filter.
//
// Returns:
//
//	A pointer to a slice of HttpResponse structs that passed the filters.
func ApplyFilters(FeroxbusterRequests *[]HttpResponse, includeFilter, excludeFilter *HttpResponse) *[]HttpResponse {
	var filteredRequests []HttpResponse

	for _, request := range *FeroxbusterRequests {
		// Check if the request matches the inclusion filter
		includeMatch := false
		if includeFilter != nil {
			includeMatch = request.DoesMatch(includeFilter)
		}

		// Check if the request matches the exclusion filter
		excludeMatch := false
		if excludeFilter != nil {
			excludeMatch = request.DoesMatch(excludeFilter)
		}

		// Decide whether to keep or remove the request
		if includeMatch && !excludeMatch {
			filteredRequests = append(filteredRequests, request)
		}
	}

	return &filteredRequests
}

func (FeroxbusterRequest *HttpResponse) DoesMatch(filter *HttpResponse) bool {
	return matchesHttpResponseFilter(FeroxbusterRequest, filter) &&
		matchesHttpHeadersFilter(&FeroxbusterRequest.Headers, &filter.Headers)
}

// private scope

// This functions might look ugly but they are pretty simple.
// I could make it clearer to read by using reflection but this relies on runtime
// where my function relies only on compiling and gets WAY faster at runtime

// matchesHttpResponseFilter filters an HttpResponse based on the fields specified in the filter struct
// by putting includeMode to True, the function ensures that the given HttpResponse includes the values in the filter struct
// otherwise, the function ensures that the given HttpResponse DOES NOT include the values in the filter struct
func matchesHttpResponseFilter(response *HttpResponse, filter *HttpResponse) bool {
	// Check each field in the filter struct
	// If a field is non-zero and differs from the corresponding field in the response,
	// set the match flag to false
	if filter.Type != "" && response.Type != filter.Type {
		return false
	}
	if filter.URL != "" && response.URL != filter.URL {
		return false
	}
	if filter.OriginalURL != "" && response.OriginalURL != filter.OriginalURL {
		return false
	}
	if filter.Path != "" && response.Path != filter.Path {
		return false
	}
	if filter.Wildcard != response.Wildcard {
		return false
	}
	if filter.Status != 0 && response.Status != filter.Status {
		return false
	}
	if filter.Method != "" && response.Method != filter.Method {
		return false
	}
	if filter.ContentLength != 0 && response.ContentLength != filter.ContentLength {
		return false
	}
	if filter.LineCount != 0 && response.LineCount != filter.LineCount {
		return false
	}
	if filter.WordCount != 0 && response.WordCount != filter.WordCount {
		return false
	}
	if filter.Extension != "" && response.Extension != filter.Extension {
		return false
	}

	return true
}

// matchesHttpHeadersFilter an HttpHeaders based on the fields specified in the filter struct
// by putting includeMode to True, the function ensures that the given HttpResponse includes the values in the filter struct
// otherwise, the function ensures that the given HttpResponse DOES NOT include the values in the filter struct
func matchesHttpHeadersFilter(headers *HttpHeaders, filter *HttpHeaders) bool {
	// Check each field in the filter struct
	// If a field is non-zero and differs from the corresponding field in the headers,
	// set the match flag to false
	if filter.AcceptRanges != "" && headers.AcceptRanges != filter.AcceptRanges {
		return false
	}
	if filter.ContentLength != "" && headers.ContentLength != filter.ContentLength {
		return false
	}
	if filter.ContentType != "" && headers.ContentType != filter.ContentType {
		return false
	}
	if filter.Date != "" && headers.Date != filter.Date {
		return false
	}
	if filter.Etag != "" && headers.Etag != filter.Etag {
		return false
	}
	if filter.LastModified != "" && headers.LastModified != filter.LastModified {
		return false
	}
	if filter.Server != "" && headers.Server != filter.Server {
		return false
	}
	if filter.Vary != "" && headers.Vary != filter.Vary {
		return false
	}

	return true
}
