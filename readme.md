# FeroxbusterReportParser

FeroxbusterReportParser is a simple package in Go made to parse feroxbuster Json outputs. It can then facilitates the analysis of datas or output.

## Install

```bash
go get gitlab.com/Kaddate/feroxbusterReportParser/pkg/feroxbusterReportParser
```

## Reports structure

Structure of feroxbuster's reports contains :
- An object called "Configuration" containing all the configuration and context of the scan.
- An object called "Statistics" containing global informations about the scan.
- An array of objects called "LogMessages" containing the logging messages displayed during a scan.
- An array of objects called "HttpResponses" containing in details all the URLs found by the scan.

```go
type FeroxbusterReport struct {
	Configuration *Configuration
	Statistics    *Statistics
	LogMessages   *[]LogMessage
	HttpResponses *[]HttpResponse
}
```

## Usage 

```go
// Retrieving the Json report
jsonInput, err := os.ReadFile(filepath)
if err != nil {
    log.Fatalln(err)
}

// Parsing the Json report
feroxbusterReport := feroxbusterReportParser.Parse(string(jsonInput))
```

Little example :

```go
for _, item := range *feroxbusterReport.HttpResponses {
    fmt.printf("%s :\n - %s\n - %d\n - %s\n", item.URL, item.Method, item.Status, item.Headers.Server)
}
```

Output :

```plain
http://fake-web.site/images/sitelogo.png :
 - GET
 - 200
 - Apache/2.4.7 (Ubuntu)
http://fake-web.site/ :
 - GET
 - 200
 - Apache/2.4.7 (Ubuntu)
http://fake-web.site/.svn :
 - GET
 - 301
 - Apache/2.4.7 (Ubuntu)
```

___

*Made wit luv :3*